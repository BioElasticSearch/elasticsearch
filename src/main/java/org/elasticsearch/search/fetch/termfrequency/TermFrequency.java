/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.search.fetch.termfrequency;

import java.util.Locale;

/**
 * Container class to hold information on terms and their vectors to be passed back to clients as part of a query.
 *
 * @author Darran Kartaschew
 */
public class TermFrequency {

    /**
     * The identifier used for the term.
     */
    public static final String TERM = "term";
    /**
     * The identifier used for the start position.
     */
    public static final String TERM_FREQUENCY = "frequency";
    /**
     * The term that this vector represents.
     */
    private String term;
    /**
     * The starting position of this term in the document.
     */
    private Long frequency;

    /**
     * Construct a term vector with default information.
     */
    public TermFrequency() {
        term = "";
        frequency = 0l;
    }

    /**
     * Construct a term vector with specified information.
     *
     * @param term The term that this TermVector represents.
     * @param frequency The frequency of the term within the document
     */
    public TermFrequency(String term, Long frequency) {
        this.term = term;
        this.frequency = frequency;
    }

    /**
     * Get the term for this TermVector.
     *
     * @return The term.
     */
    public String getTerm() {
        return term;
    }

    /**
     * Set the term for this TermVector.
     *
     * @param term The term that this vector represents.
     */
    public void setTerm(String term) {
        this.term = term;
    }

    /**
     * Get the frequency for this term in the document.
     *
     * @return The starting position of the term.
     */
    public Long getFrequency() {
        return frequency;
    }

    /**
     * Set the frequency for this term in the document.
     *
     * @param startPosition The starting position of the term.
     */
    public void setFrequency(Long frequency) {
        this.frequency = frequency;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (this.term != null ? this.term.hashCode() : 0);
        hash = 19 * hash + (this.frequency != null ? this.frequency.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TermFrequency other = (TermFrequency) obj;
        if ((this.term == null) ? (other.term != null) : !this.term.equals(other.term)) {
            return false;
        }
        if (this.frequency != other.frequency && (this.frequency == null || !this.frequency.equals(other.frequency))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format(Locale.UK, "TermVector { term=%s, frequency=%d }",
                term, frequency);
    }

    /**
     * Return the representation of this Term Frequency Object in JSON notation.
     *
     * @return A string that represents this TermVector in JSON notation.
     */
    public String toJSON() {
        return String.format(Locale.UK, " { \"term\" : \"%s\", \"frequency\" : \"%d\" }",
                term, frequency);
    }
}
