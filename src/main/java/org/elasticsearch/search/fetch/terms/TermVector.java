/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.search.fetch.terms;

import java.util.Locale;

/**
 * Container class to hold information on terms and their vectors to be passed back to clients as part of a query.
 *
 * @author Darran Kartaschew
 */
public class TermVector {

    /**
     * The identifier used for the term.
     */
    public static final String TERM = "term";
    /**
     * The identifier used for the start position.
     */
    public static final String START_POSITION = "start_position";
    /**
     * The identifier used for the end position.
     */
    public static final String END_POSITION = "end_position";
    /**
     * The identifier used for the next position.
     */
    public static final String NEXT_POSITION = "next_position";
    /**
     * The term that this vector represents.
     */
    private String term;
    /**
     * The starting position of this term in the document.
     */
    private Long startPosition;
    /**
     * The ending position of this term in the document.
     */
    private Long endPosition;
    /**
     * The next position of this term in the document.
     */
    private Long nextPosition;

    /**
     * Construct a term vector with default information.
     */
    public TermVector() {
        term = "";
        startPosition = -1l;
        endPosition = -1l;
        nextPosition = -1l;
    }

    /**
     * Construct a term vector with specified information.
     *
     * @param term The term that this TermVector represents.
     * @param startPosition The starting position of the term in the document. (Use -1 for unknown).
     * @param endPosition The ending position of the term in the document. (Use -1 for unknown).
     * @param nextPosition The next position of the term in the document. (Use -1 for unknown).
     */
    public TermVector(String term, Long startPosition, Long endPosition, Long nextPosition) {
        this.term = term;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.nextPosition = nextPosition;
    }

    /**
     * Get the term for this TermVector.
     *
     * @return The term.
     */
    public String getTerm() {
        return term;
    }

    /**
     * Set the term for this TermVector.
     *
     * @param term The term that this vector represents.
     */
    public void setTerm(String term) {
        this.term = term;
    }

    /**
     * Get the starting position for this term in the document.
     *
     * @return The starting position of the term.
     */
    public Long getStartPosition() {
        return startPosition;
    }

    /**
     * Set the starting position for this term in the document.
     *
     * @param startPosition The starting position of the term.
     */
    public void setStartPosition(Long startPosition) {
        this.startPosition = startPosition;
    }

    /**
     * Get the ending position for this term in the document.
     *
     * @return The starting position of the term.
     */
    public Long getEndPosition() {
        return endPosition;
    }

    /**
     * Set the ending position for this term in the document.
     *
     * @param endPosition The starting position of the term.
     */
    public void setEndPosition(Long endPosition) {
        this.endPosition = endPosition;
    }

    /**
     * Get the next position for this term in the document.
     *
     * @return The starting position of the term.
     */
    public Long getNextPosition() {
        return nextPosition;
    }

    /**
     * Set the next position for this term in the document.
     *
     * @param nextPosition The starting position of the term.
     */
    public void setNextPosition(Long nextPosition) {
        this.nextPosition = nextPosition;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (this.term != null ? this.term.hashCode() : 0);
        hash = 19 * hash + (this.startPosition != null ? this.startPosition.hashCode() : 0);
        hash = 19 * hash + (this.endPosition != null ? this.endPosition.hashCode() : 0);
        hash = 19 * hash + (this.nextPosition != null ? this.nextPosition.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TermVector other = (TermVector) obj;
        if ((this.term == null) ? (other.term != null) : !this.term.equals(other.term)) {
            return false;
        }
        if (this.startPosition != other.startPosition && (this.startPosition == null || !this.startPosition.equals(other.startPosition))) {
            return false;
        }
        if (this.endPosition != other.endPosition && (this.endPosition == null || !this.endPosition.equals(other.endPosition))) {
            return false;
        }
        if (this.nextPosition != other.nextPosition && (this.nextPosition == null || !this.nextPosition.equals(other.nextPosition))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format(Locale.UK, "TermVector { term=%s, startPosition=%d, endPosition=%d, nextPosition=%d }",
                term, startPosition, endPosition, nextPosition);
    }

    /**
     * Return the representation of this TermVector in JSON notation.
     *
     * @return A string that represents this TermVector in JSON notation.
     */
    public String toJSON() {
        return String.format(Locale.UK, " { \"term\" : \"%s\", \"start_position\" : \"%d\", \"end_position\" : \"%d\", \"next_position\" : \"%d\" }",
                term, startPosition, endPosition, nextPosition);
    }
}
