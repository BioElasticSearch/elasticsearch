/*
 * Licensed to Elastic Search and Shay Banon under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. Elastic Search licenses this
 * file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.elasticsearch.search.fetch.terms;

import com.google.common.collect.ImmutableMap;
import org.apache.lucene.index.DocsAndPositionsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.search.SearchParseElement;
import org.elasticsearch.search.fetch.FetchPhaseExecutionException;
import org.elasticsearch.search.fetch.FetchSubPhase;
import org.elasticsearch.search.internal.InternalSearchHit;
import org.elasticsearch.search.internal.SearchContext;

import java.util.*;

/**
 * @author Darran Kartaschew
 */
public class TermsFetchSubPhase implements FetchSubPhase {

    @Override
    public Map<String, ? extends SearchParseElement> parseElements() {
        return ImmutableMap.of("terms", new TermsParseElement());
    }

    @Override
    public boolean hitsExecutionNeeded(SearchContext context) {
        return false;
    }

    @Override
    public void hitsExecute(SearchContext context, InternalSearchHit[] hits) throws ElasticsearchException {
    }

    @Override
    public boolean hitExecutionNeeded(SearchContext context) {
        return context.terms();
    }

    @Override
    public void hitExecute(SearchContext context, HitContext hitContext) throws ElasticsearchException {
        try {
            List<TermVector> termVectors = new ArrayList<TermVector>();
            termVectors.add(new TermVector());
            /*
             * Get our original terms, as Lucene only give you all term vectors for a given document 
             * (and we have to manually match our own search terms with the terms Lucene gives us. (very, very sucky).
             */
            Set<Term> termCollection = new LinkedHashSet<Term>();
            context.query().extractTerms(termCollection);

            // we use the top level doc id, since we work with the top level searcher
            if (hitContext.reader() != null) {
                Terms termVector;
                try {
                    termVector = hitContext.reader().getTermVector(hitContext.hit().docId(), "content");
                } catch (IndexOutOfBoundsException ex) {
                    termVector = null; // just set the term vector to null, and exit.
                }
                if (termVector == null) {
                    hitContext.hit().termVectors(termVectors);
                    return;
                }

                TermsEnum luceneTermsEnum = termVector.iterator(null);
                //BytesRef ref;
                DocsAndPositionsEnum docsAndPositions = null;
                termVectors.clear();

                // Cycle through all terms in our query, and seek to the required 
                Iterator<Term> terms = termCollection.iterator();
                while (terms.hasNext()) {
                    Term term = terms.next();
                    if (luceneTermsEnum.seekExact(term.bytes())) {
                        docsAndPositions = luceneTermsEnum.docsAndPositions(null, docsAndPositions);
                        // beware that docsAndPositions will be null if you didn't index positions
                        if (docsAndPositions.nextDoc() != 0) {
                            throw new AssertionError();
                        }

                        final int freq = docsAndPositions.freq(); // number of occurrences of the term
                        for (int i = 0; i < freq; ++i) {
                            final long nposition = docsAndPositions.nextPosition();
                            final long sposition = docsAndPositions.startOffset();
                            final long eposition = docsAndPositions.endOffset();
                            // 'position' is the i-th position of the current term in the document
                            termVectors.add(new TermVector(term.text(), sposition, eposition, nposition));
                        }
                    } else {
                        // Not found?
                        termVectors.add(new TermVector(term.text(), -1l, -1l, -1l));
                    }
                }
            }
            hitContext.hit().termVectors(termVectors);
        } catch (Exception e) {
            throw new FetchPhaseExecutionException(context, "Failed to get terms for doc [" + hitContext.hit().type() + "#" + hitContext.hit().id() + "]", e);
        }
    }
}
